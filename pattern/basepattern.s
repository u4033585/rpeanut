0x0100 :  load #0 R6  ; R6 is used to store the x
          load #0 R7  ; R7 stores the y
loop :    
          ;=========================================
          ; Modify the code in this section.
          ; Set the value of register R0 to 1 if you want
          ; pixel at location (R6,R7) to be white and set it 
          ; to zero if you want it black.
          ; You can use registers R0, R1,
          ; R2, R3, R4 for your calculations.
 
         
         
          load  #1 R0  ; set register R0 to 1 - makes the screen all white
 
          ;========================================= 
          
         
          and #1 R0 R2
          jumpz R2 skipdraw
          mult R7 #6 R5
          div R6 #32 R4
          add R5 R4 R5
          load R5 #0x7c40 R4
          mod R6 #32 R3
          rotate R3 R2 R3
          or R3 R4 R4
          store R4 #0x7c40 R5
skipdraw: add R6 #1 R6
          sub R6 #192 R4
          jumpnz R4 loop
          add R7 #1 R7
          sub R7 #160 R4
          load #0 R6
          jumpnz R4 loop
          halt

